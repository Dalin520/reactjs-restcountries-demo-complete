import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import MyNavbar from './components/MyNavbar';
import MyFooter from './components/Footer';
import Home from './components/Home';
import Filter from './components/Filter';
import Detail from './components/Detail';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <MyNavbar/>
          <Filter/>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/detail/:id' component={Detail}/>
            <Route path='/search/:by/:keyword' component={Home}/>
            <Route path='/cate/:cate' component={Home}/>
          </Switch>

          <MyFooter/>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
