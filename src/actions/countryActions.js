import {API} from '../utils/API'
import {actionType} from './actionType'

export const fetchCountry = (by,txt,cate) =>{
    return(dispatch) =>{
        fetch(
            by || txt? 
            by==='name'? API.url+'/name/'+txt: API.url+'/capital/'+txt
            : cate? API.url+'/region/'+cate :API.url+'/all'
            )
        .then(res=> res.json())
        .then(res=>dispatch({
            type: actionType.FETCH_COUNTRY,
            payload: res
        }))
    }
}

export const countryDetail = (id) => {
    return(dispatch) =>{
        fetch(API.url+'/callingcode/'+id)
        .then(res =>res.json())
        .then(res=>{
            let obj = Object.assign({}, ...res) 
            console.log('====cD: ',obj)
            dispatch({
            type: actionType.COUNTRY_DETAIL,
            payload: obj
        })})
    
    }
}

export const countryCategory = () =>{
    return(dispatch) =>{
        // fetch(API.url+'/callingcode/')
        // .then(res =>res.json())
        // .then(res=>{
        //     let obj = Object.assign({}, ...res) 
        //     console.log('====cD: ',obj)
        //     dispatch({
        //     type: actionType.COUNTRY_DETAIL,
        //     payload: obj
        // })})
    
    }
}