import React, { Component } from 'react';
import {connect} from 'react-redux'
import {countryDetail} from '../actions/countryActions'
import {Card,CardTitle,Col,Row,Container, CardPanel} from 'react-materialize'

class Detail extends Component {

    componentDidMount(){
        let id = this.props.match.params.id
        this.props.countryDetail(id);
    }

    render() {
        console.log('=====detail',this.props)
        const {country} = this.props
        
        return (
            <Container> 
                <Row>
                    <Col m={12} s={12}>
                        <CardPanel>
                            <Card horizontal header={<CardTitle image={country.flag}></CardTitle>}>
                            <h3>{country.name}</h3>
                            <label>Capital : </label><span>{country.capital}</span><br/>
                            <label>Region : </label><span>{country.region}</span><br/>
                            <label>Subregion : </label><span>{country.subregion}</span><br/>
                            <label>Population : </label><span>{country.population} people</span><br/>
                            <label>Area : </label><span>{country.area} m<sup>2</sup></span><br/>
                            <label>Native Name : </label><span>{country.nativeName}</span><br/>
                            
       
                            </Card>
                        </CardPanel>
                    </Col>
                </Row>
            </Container>
        );
    }
}

const mapStateToProps = (state) =>{
    return{
        country: state.country.countryDetail
    }
}

export default connect(mapStateToProps,{countryDetail}) (Detail);