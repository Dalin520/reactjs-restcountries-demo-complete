import React, { Component } from 'react';
import {Button,Container,CardPanel,Row,Input} from 'react-materialize'
import {Link} from 'react-router-dom'

class Filter extends Component {
    state={
        text:'',
        select:''
    }
    handleOnChange(e){
        this.setState({
            [e.target.name]:e.target.value
        })
    }
    componentDidMount(){
        this.setState({
            select:'name'
        })
    }
    render() {
        return (
            <Container>
                <br/>
                <CardPanel>
                <Row>
                    <div className='col s12'>
                        <Input m={8} s={7} label="Search.." value={this.state.text} name='text' onChange={(e)=>this.handleOnChange(e)} />
                        <Link to={ this.state.text ? '/search/'+this.state.select+"/"+this.state.text : window.location.pathname} >
                            <Button style={{marginTop: 15,padding: 0,}} floating small className='black' waves='light' icon='search'></Button>
                        </Link>
                        <Input s={3} type='select' label='Search by' name='select' value={this.state.select} onChange={(e)=>this.handleOnChange(e)}>
                        <option value='name'>name</option>
                        <option value='capital'>capity</option>
                        </Input>
                    </div>
                  
                </Row>
                </CardPanel>
                
            </Container>
        );
    }
}

export default Filter;