import React, { Component } from 'react';
import {Footer} from 'react-materialize'

class MyFooter extends Component {

    render() {
        return (
            <Footer copyrights="Januray 2019"
            className='grey darken-4'
            >
                <h5 className="white-text">ReactJS Demo Complete</h5>
            </Footer>
        );
    }
}

export default MyFooter;