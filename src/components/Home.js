import React, { Component } from 'react';
import {Card,CardTitle,Col,Row,Preloader} from 'react-materialize'
import {Link} from 'react-router-dom'
import {connect} from 'react-redux'
import {fetchCountry} from '../actions/countryActions'

class Home extends Component {

   componentDidMount(){ 
    this.props.fetchCountry()
   }

   componentDidUpdate(){
    let txt =  this.props.match.params.keyword
    let by = this.props.match.params.by
    let cate = this.props.match.params.cate
    cate? this.props.fetchCountry(by,txt,cate):
       this.props.fetchCountry(by,txt)
       
   }
    render() {
        const {country} = this.props
        
        return (
            <div className='container'>
                <Row>
               {
                   country.length?
                   country.map(item=>
                    (
                    <Link to={'/detail/'+item.callingCodes[0]} key={item.callingCodes[0]+'/1'+item.population+'/'+item.name}>
                    <Col m={4} s={6}>
                        <Card header={<CardTitle image={item.flag} waves='light'/>}
                            title={item.name}
                        >
                        <p>population: {item.population}</p>
                        </Card>
                    </Col>
                    </Link>
                   ))
                   :
                   <Col s={12} className='center'><div className='padding'><Preloader flashing/></div></Col>
               }
             
               </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) =>{
    return {
        country : state.country.countries
    }
}

export default connect(mapStateToProps,{fetchCountry}) (Home);