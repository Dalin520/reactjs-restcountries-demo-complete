import React, { Component } from 'react';
import {Navbar,Container,NavItem} from 'react-materialize'
import {NavLink} from 'react-router-dom'

class MyNavbar extends Component {
    state={//['Africa, Americas, Asia, Europe, Oceania']
        cate:[
            {id: 1,name:'Africa'},
            {id: 2,name:'Americas'},
            {id: 3,name:'Asia'},
            {id: 4,name:'Europe'},
            {id: 5,name:'Oceania'},
        ]
    }
    render() {
        return (
            <Container grey>
                <Navbar brand={<h5 style={{paddingLeft:10,marginTop:0}}><i className='fa fa-flag'> RC</i></h5>}  right style={{backgroundColor:'rgba(0,0,0,0.8)'}}>
                    <NavItem ><NavLink to='/' >Home</NavLink></NavItem>
                    {
                        this.state.cate.map((item,index)=>(
                            <NavItem key={index}><NavLink to={'/cate/'+item.name} >{item.name}</NavLink></NavItem> 
                        ))
                    }
                </Navbar>
            </Container>
        );
    }
}

export default MyNavbar;   