import {actionType} from '../actions/actionType'
const iniState={
    countries:[],
    countryDetail:{},
    state: false
}

const countryReducer = (state = iniState, action) =>{
    switch(action.type){
        case actionType.FETCH_COUNTRY:
            return{...state, countries:action.payload}
        case actionType.COUNTRY_DETAIL:
            return{...state,countryDetail:action.payload}

        default : return state
    }
}

export default countryReducer