import {createStore, compose, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import { rootReducer } from '../reducers/rootReducer'

const initState={}
const middlWare = [thunk]

export const centralStore= createStore( rootReducer, initState , compose(applyMiddleware(...middlWare)) )